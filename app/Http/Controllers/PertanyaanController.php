<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Question\Question;

class PertanyaanController extends Controller
{
    public function index(){
        $questions = DB::table('pertanyaan')->get();
        // dd($questions);
        return view('questions.index',compact('questions'));
    }
    public function create(){
        return view('questions.create');
    }
    public function store(Request $request){
        // dd($request->all());
        $query = DB::table('pertanyaan')->insert([
            "judul" => $request["questionTitle"],
            "isi" => $request["questionBody"]
        ]);
        return redirect('pertanyaan')->with('success',"Question Asked!");
    }
    public function show($id){
        $question = DB::table('pertanyaan')->where('id', $id)->first();
        // dd($question);
        return view('questions.show',compact('question'));
    }
    public function edit($id){
        $question = DB::table('pertanyaan')->where('id', $id)->first();
        return view('questions.edit',compact('question'));
    }
    public function update($id,Request $request){
        $query = DB::table('pertanyaan')
                ->where('id',$id)
                ->update([
                    "judul" => $request["questionTitle"],
                    "isi" => $request["questionBody"]
                ]);
        return redirect('/pertanyaan')->with('success','Updated!');
    }
    public function destroy($id){
        $query = DB::table('pertanyaan')->where('id',$id)->delete();
        return redirect('/pertanyaan')->with('success','Deleted!');
    }
}
