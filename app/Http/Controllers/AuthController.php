<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PhpParser\Node\Stmt\Echo_;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }

    public function kirim(Request $request){
        $namaDepan = $request->depan;
        $namaBelakang = $request->belakang;
        return view('selamat',compact('namaDepan','namaBelakang'));
    }
}
