<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Selamat!</title>
</head>
<body>
    <h1>Selamat Datang {{$namaDepan}} {{$namaBelakang}} !</h1>
    <p><b>Terima kasih telah bergabung di Sanberbook. Social Media kita bersama!</b></p>
</body>
</html>