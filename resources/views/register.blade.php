<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/kirim" method="POST">
    @csrf
        <label for="first">First name:</label><br>
        <input type="text" name="depan"><br><br>
        
        <label for="last">Last name:</label><br>
        <input type="text" name="belakang"><br><br>
        
        <label for="gender">Gender:</label><br>
        <input type="radio" name="gender">Male <br>
        <input type="radio" name="gender">Female <br>
        <input type="radio" name="gender">Other <br><br>
        
        <label for="nationality">Nationality:</label><br>
        <select name="" id="nationality">
            <option>Indonesian</option>
            <option>Singaporean</option>
            <option>Australian</option>
            <option>Malaysian</option>
        </select><br><br>
        
        <label for="language">Language Spoken:</label><br>
        <input type="checkbox">Bahasa Indonesia <br>
        <input type="checkbox">English <br>
        <input type="checkbox">Other <br><br>

        <label for="bio">Bio:</label><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>

        <input type="submit" value="Sign Up">
    </form>
</body>
</html>