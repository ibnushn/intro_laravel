@extends('layouts.master')
@section('title')
    <h1 style="padding: 20px; text-align:center;">Your Question Details</h1>
@endsection
@section('content')
<div class="m-4">
    <h2>{{ $question->judul }}</h2>
    <p>{{ $question->isi }}</p>
</div>
@endsection