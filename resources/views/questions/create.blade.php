@extends('layouts.master')
@section('title')
    <h1 style="padding: 20px; text-align:center;">Create Your Question Here</h1>
@endsection
@section('content')
<div class="card card-primary m-2">
    <div class="card-header">
      <h3 class="card-title">Question Form</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/pertanyaan" method="POST">
    @csrf
      <div class="card-body">
          <div class="form-group">
              <label for="questionTitle">Title</label>
              <input type="text" class="form-control" id="questionTitle" name="questionTitle" placeholder="Title of your question">
            </div>
            <div class="form-group">
                <label for="questionBody">Question</label>
                <input type="text" class="form-control" id="questionBody" name="questionBody" placeholder="What is is that you really want to ask?">
            </div>
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit Question</button>
      </div>
    </form>
  </div>
@endsection