@extends('layouts.master')
@section('title')
    <h1 style="padding: 20px; text-align:center;">Edit your Question</h1>
@endsection
@section('content')
<div class="card card-primary m-2">
    <div class="card-header">
    <h3 class="card-title">Edit Question {{$question->id}}</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/pertanyaan/{{$question->id}}" method="POST">
    @csrf
    @method('PUT')
      <div class="card-body">
          <div class="form-group">
              <label for="questionTitle">Title</label>
          <input type="text" class="form-control" id="questionTitle" name="questionTitle" value="{{$question->judul}}" placeholder="Title of your question">
            </div>
            <div class="form-group">
                <label for="questionBody">Question</label>
                <input type="text" class="form-control" id="questionBody" name="questionBody" value="{{$question->isi}}" placeholder="What is is that you really want to ask?">
            </div>
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
    </form>
  </div>
@endsection