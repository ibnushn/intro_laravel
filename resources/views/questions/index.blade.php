@extends('layouts.master')
@section('title')
    <h1 style="padding: 20px; text-align:center;">List of Questions</h1>
@endsection
@section('content')
<div class="card m-3">
    <div class="card-header">
        <h3 class="card-title">Questions</h3>
        <a href="{{asset('pertanyaan/create')}}" class="btn btn-primary float-right">Ask Here!</a>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        @if (session('success'))
        <div class="alert alert-success">{{session('success')}}</div>
        @endif
      <table class="table table-bordered">
        <thead>                  
          <tr>
            <th style="width: 4%">#</th>
            <th style="width: 30%">Tittle</th>
            <th style="width: 56%">Body</th>
            <th style="width: 10%">Actions</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($questions as $key => $question)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $question->judul }}</td>
                    <td>{{ $question->isi }}</td>
                    <td style="display: flex;">
                        <a href="/pertanyaan/{{$question->id}}" class="btn btn-info btn-sm mr-1">show</a>
                        <a href="/pertanyaan/{{$question->id}}/edit" class="btn btn-default btn-sm">edit</a>
                        <form action="/pertanyaan/{{$question->id}}" method="POST">
                          @csrf
                          @method('DELETE')
                        <input type="submit" value="delete" class="btn btn-danger btn-sm ml-1">
                        </form>
                    </td>
                    
                </tr>
            @endforeach
          {{-- <tr>
            <td>1.</td>
            <td>Update software</td>
            <td>
              <div class="progress progress-xs">
                <div class="progress-bar progress-bar-danger" style="width: 55%"></div>
              </div>
            </td>
            <td><span class="badge bg-danger">55%</span></td>
          </tr> --}}
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
    {{-- <div class="card-footer clearfix">
      <ul class="pagination pagination-sm m-0 float-right">
        <li class="page-item"><a class="page-link" href="#">«</a></li>
        <li class="page-item"><a class="page-link" href="#">1</a></li>
        <li class="page-item"><a class="page-link" href="#">2</a></li>
        <li class="page-item"><a class="page-link" href="#">3</a></li>
        <li class="page-item"><a class="page-link" href="#">»</a></li>
      </ul>
    </div>
  </div> --}}
@endsection